﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_3
{
    class generic
    {
        public static void insertionSort <T>(T[] arr, T n) where T : IComparable
        {
            int  key, j;
            for (int i = 1; i.CompareTo(n) < 0; i++)
            {
                
                key = arr[i].CompareTo(arr[i]);
                j = i - 1;
                

                /* Move elements of arr[0..i-1], that are
                   greater than key, to one position ahead
                   of their current position */
                while ((j >= 0 && arr[j].CompareTo(arr[j]) > key))
                {
                    arr[j + 1] = arr[j];
                    j = j - 1;
                }
                arr[j + 1] = key;
            }
        }
    }
}
